r""" Utilities to interact wit Abaqus input files.

This script defines parsing tools according to Abaqus conventions.
We want to read an inp file and turn it into a python data structure, and conversely.

Notes
------

The inp format specifications is provided by Dassault systems.

We turn it into a list of tuples on length 3 containing
1. the key word name (str).
2. Its contents: a dict containing 'parameters' and 'data'
   with default values None and [], respectively.
3. Its type: a string, can be: 'KW', SS, SE (for keyword, section starts, section ends)

For example:
```
[ ('keyw name 1', {'parameters':{'p1':1. 'P2':'b'}, 'data': [1, 45, 'str', 2.3e6] }, 'KW') #KW1
  ('keyw name 2', {'parameters':None              , 'data': [] }                   , 'KW') #KW2
# ... etc
]
# (<---- 1 ---->, <------------------  2  ---------------------------------------->, <3> )
```

References
----------

"""

import logging
import fileinput

def read(inp_path:str='stdin'):
    """Returns a list of dicts

    Arguments
    ---------
    inp_path: str
        The path to the input file

    Returns:
    --------
    inp list: tuple of lists
        One element per keyword, composed of (<type>:str, <name>:str, <contents>: dict)
        The <contents> element is a dictionary with 2 keys 'parameters' and 'data'.

    """
    logging.info('Reading INP file: %s', inp_path)

    inp_list = list()

    # Make a first run to find SECTIONS
    # That is Keywords that start with '*End '
    sectionset = set()
    with fileinput.input(files=(inp_path)) as inp_file:
        for line in inp_file:
            # Sanitize
            line = " ".join(line.split())#remove \n, \t etc.
            if line.strip().lower().startswith('*end'):
                sectionset.add(line.lower().removeprefix('*end '))

    # Run again to list keywords

    with fileinput.input(files=(inp_path)) as inp_file:
        for line in inp_file:

            #------------------------------------------
            # Sanitize
            line = " ".join(line.split()) #line = line.replace(" ", "").lower()

            #------------------------------------------
            #Ignore if comment
            if line.strip().startswith('**'):
                pass

            #------------------------------------------
            # Else deal with it: either section, keyword or data
            elif line.strip().startswith('*'):
                keyw, param = _read_keyword_line(line)
                # Is it a section?
                if keyw.lower().startswith('end'):
                    inp_list += [(
                        keyw.upper().removeprefix('END ').strip(),
                        dict( parameters=param, data=[]),
                        'SE',#'SE: '+#
                        )]
                elif keyw.lower() in sectionset:
                # then it's a section
                    inp_list += [(
                        keyw.upper().strip(),
                        dict( parameters=param, data=[]),
                        'SS',#'SS: '+#
                        )]
                else:
                # Then is it a keyword line
                    inp_list += [(
                        keyw.capitalize(),
                        dict( parameters=param, data=[]),
                        'KW',#'KW: '+#
                        )]

            #------------------------------------------
            # Then it is data: append data to our last line
            else:
                inp_list[-1][1]['data'] += _read_data_line(line)

    return inp_list

def _read_keyword_line(line):
    """Returns INP keyword and list of parameters from a string.
    """
    comma_list = line.removeprefix('*').split(',')
    keyword=comma_list.pop(0)
    if len(comma_list) == 0:
        return keyword, None
    parameters=dict()
    for _ in range(len(comma_list)):
        par_val = comma_list.pop(0).split('=')
        # Actually the first was the name
        par_name = par_val.pop(0).strip()
        #parameters[par_name] = parse_val(par_val)
        parameters[par_name] = parse_val(par_val[0]) if len(par_val) else par_val
    return keyword, parameters

def _read_data_line(line:str):
    """Returns list of values after processing a CSV string
    """
    str_list = line.split(',')
    val_list = []
    for txt in str_list:
        val_list += [parse_val(txt)] if len(txt) else []
    return val_list

def parse_val(txt:str):
    """ Return value from a string if it represents a value.

    taken from: https://stackoverflow.com/a/27117539
    """
    txt=txt.strip()
    try:
        return int(txt)
    except ValueError:
        pass
    try:
        return float(txt)
    except ValueError:
        pass
    return txt


####################################################################################################
####################################################################################################

PADDED_LINE = '**'+' '*(72-2)
LINE_LEN = len(PADDED_LINE)

def _inp_keyword_to_str(kwname:str, kwdict:dict, kwtype:str='KW' ):
    """Returns the text to write to an inp file

    Arguments
    ---------
    kwname : str
        the inp-keyword
    kwdict : dict
        the corresponding dictionnary with entries:
        'parameters' : dict
            possibly empty dict
        'data' :
    """
    # Deal with different types
    if kwtype != 'KW':
        raise Exception('Only kwtype "KW" is implemented')

    # Create string

    #'** ------------------------------------------------------------*'
    kwstr = PADDED_LINE

    ##'** KEYWORD start ----------------------------------------------*'
    #kwstr += '\n**'+(kwname.upper()+' start ').ljust(LINE_LEN-2, '-')

    #'** Comments comments comments                                  *'
    if 'comments' in kwdict.keys():
        kwstr +='\n** '+kwdict['comments']

    #*Keyword. parm1=val1, param2=val2, ...
    kwstr += '\n*'+kwname.capitalize()
    for parname, parval in kwdict['parameters'].items():
        kwstr += ', {}={}'.format(parname, str(parval))
    # Define DATA LINES
    kwstr += '\n '
    len_dat = 1
    for dat in kwdict['data']:
        if len_dat+len(str(dat))+2>=LINE_LEN:
            kwstr += '\n '
            len_dat = 1
        kwstr += str(dat)+', '
        len_dat += len(str(dat))+2

    # CLosing
    ##'** KEYWORD end  -----------------------------------------------*'
    #kwstr += '\n**'+(kwname.upper()+' end   ').ljust(LINE_LEN-2, '-')

    #'** ------------------------------------------------------------*'
    kwstr += '\n'+PADDED_LINE

    return kwstr

def write(kwname:str, kwdict:dict, kwtype='KW',  **kvargs):
    """Write keyword contents to an Abaqus input file.

    Arguments
    ---------
    kwname : str
        the keyword name to write in inp file
    kwdict : dict
        with predefined entries 'data', 'parameters' the least.
    open_mode : str
        the argument passed to open (default 'x' throws an exception if file exists)
    kvargs : dict
        expected items are:
        - `file_name`=kwname, the file to write on
        - 'open_mode'='x' parameters passed to the `open` function.

    """
    kvargs.setdefault('open_mode', 'x')
    kvargs.setdefault('file_name', kwname.lower().replace(' ', '_')+'.inp')

    logging.info('Writing %s to INP file %s', kwname, kvargs['file_name'])

    # Safely create and start writing
    with open(kvargs['file_name'], kvargs['open_mode'],) as uminp:
        uminp.write(_inp_keyword_to_str(kwname, kwdict, kwtype))
